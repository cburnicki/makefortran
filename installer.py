import os
import sys
path = os.path.dirname(os.path.abspath(__file__))

# build makeFortran starter

starter = '#!/usr/bin/python\n'
starter += '# -*- coding: utf-8 -*-\n'
starter += 'import sys\n'

# register the current directory for the makefortran module import
starter += 'sys.path.append(\'' + path + '\')\n'

try:
    f = open('starterTemplate.py', 'r')
    starter += f.read()
    f.close()

    f = open(sys.argv[1]+'/makeFortran', 'w')
    f.write(starter)
    f.close()

except IOError as e:
    print 'ERROR: ' + str(e.errno) + " " + e.message
