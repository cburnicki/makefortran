# makeFortran
A very simple build tool that creates a makefile for your fortran project. All binary files will be generated in the source file folder. Modules in subdirectories are not supported.

Please send bug reports and ideas to christoph.burnicki@gmail.com

## Installation

Copy or clone the repositories content to any destination on your filesystem. Run install.sh to install the program.

## Usage

Suppose you have a program `foo.f90` in `~/myproject/`. The modules used by your program should also be located in this directory. Run `makeFortran foo -d` to create your makeFile. The makefile will create an executable `foo.exe`. The `-d` flag wil automatically set a number of debug flags for your fortran compiler. 
