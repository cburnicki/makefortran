import os
import makeFortran
import sys


def printHelp():

    print ('-h, --help\t\tshow this help.\n'+
           '-cf [flag1,flag2,...]\tuse compiler flags\n'+
           '-lf [flag1,flag2,...]\tuse link flags\n'+
           '-d \t\tsets debug flags: -Wall -fcheck=bounds -g\n')

    exit()

makeFortran.target = sys.argv[1]
makeFortran.target = os.getcwd()+"/"+makeFortran.target

for i, arg in enumerate(sys.argv[2:]):

    if arg == '-h' or arg == '--help':
        printHelp()

    elif arg == 'clean' or arg == '-c' or arg == '--clean':
        makeFortran.cleanup()
        exit()

    elif arg == '-d':
        makeFortran.compilerFlags = ['-Wall', '-fcheck=bounds', '-g']
        makeFortran.linkFlags = ['-Wall', '-fcheck=bounds', '-g']

    elif arg == '-cf':
        makeFortran.compilerFlags = sys.argv[i+2].split(',')

    elif arg == '-lf':
        makeFortran.linkFlags = sys.argv[i+2].split(',')

makeFortran.cwd = os.getcwd()
makeFortran.run()
